#!/bin/bash
set -e -x

#Given a params file and an index of the scan as input argument,
#this script would generate the WAVELET Stats for the scan

SESSION=$1
SCAN_ID=$2
BUILD_DIR=$3
# my_sge_task_id=$1
# paramsFile=$2
isStructural=no
# source $paramsFile;
QC_DIR=/nrgpackages/tools.release/intradb/functional-qc

source /nrgpackages/scripts/epd-python_setup.sh

wavelet_dir=$BUILD_DIR/wavelet/$SCAN_ID
indir=${BUILD_DIR}/NIFTI/${SCAN_ID}
inpattern="*.nii.gz"

pushd $indir
  python $QC_DIR/bin/WaveletStatistics.py -D $indir -N *.nii.gz -O $wavelet_dir
popd


plot_functional=$QC_DIR/bin/generate_plots_functional.csh

mkdir -p $wavelet_dir

pushd $wavelet_dir
	if [ $isStructural = no ] ; then
	 $plot_functional $SESSION $wavelet_dir wavelet_dir $SCAN_ID
	fi
popd

exit 0

