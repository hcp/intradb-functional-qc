#!/bin/bash
set -e -x

USER=$1
PASS=$2
SCAN_URI=$3
SESSION=$4
SCAN=$5
RESOURCE=$6
BUILD_DIR=$7
JSESSIONID=$8

OLD_IFS="$IFS"

HOST=`echo $SCAN_URI | sed -e "s/\/data\/projects.*$//"`
SCAN_URI=`echo "$SCAN_URI" | sed -e "s/^.*\/data\/projects/\/data\/projects/"`
PROJECT=`echo "$SCAN_URI" | sed -e "s/^.*\/data\/projects\///" -e "s/\/.*$//"`
echo  "Determine ALT_HOST to try to work around http vs https firewall issues.  Always try to use http when possible."
if [[ "$HOST" =~ ^https:.* ]] ; then
	ALT_HOST="$HOST"
	HOST=$(echo "$ALT_HOST" | sed -e "s/https:/http:/" -e "s/\/\/[^\/]*/&:8080/")	
elif [[ "$HOST" =~ ^http:.* ]] ; then
	ALT_HOST=$(echo "$HOST" | sed -e "s/http:/https:/" -e "s/:8080//")	
else
	ALT_HOST="$HOST"
fi
echo "HOST=${HOST}, ALT_HOST=${ALT_HOST}"

source /opt/app/intradb/bash-utilities/common_functions.sh

echo "Calling refresh_jsession"
HOST_SESS=$(refresh_jsession)
IFS="," read -a HSARR <<< $HOST_SESS
JSESSIONID=${HSARR[0]}
HOST=${HSARR[1]}
IFS="$OLD_IFS"

uri=$HOST/xapi/pipelineControlPanel/project/$PROJECT/pipeline/Level2QC/entity/$SESSION/group/ALL/setValues?status=RUNNING
curl -s -k --cookie "JSESSIONID=$JSESSIONID" $uri -X POST -H "Content-Type: application/json" &> /dev/null || true

# Set up build space
zip_dir=$BUILD_DIR/zips
unzip_dir=$BUILD_DIR/$RESOURCE/$SCAN
mkdir -p $zip_dir
mkdir -p $unzip_dir

# uri=$HOST/data/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN/resources/DICOM/files?format=zip
uri=${HOST}${SCAN_URI}/resources/$RESOURCE/files?format=zip
zip_file=${SESSION}_${SCAN}_${RESOURCE}.zip
doCurl -s -k --cookie "JSESSIONID=$JSESSIONID" -w "%{http_code}" $uri -o $zip_dir/$zip_file
# curl -f -s -k --cookie "JSESSIONID=$JSESSIONID" $uri > $zip_dir/$zip_file
unzip -o -j $zip_dir/$zip_file -d $unzip_dir

